const mylib = {
    add: (a, b) => {
        const sum = a + b;
        return sum;
    },
    subtract: (a, b) => {
        return a - b;
    },
    divide: (dividend, divisor) => dividend / divisor,
    
    multiply: function(a, b) {
        return a * b;
    },
};

module.exports = mylib;