const expect = require('chai').expect;
const { describe } = require('mocha');
const mylib = require('../src/mylib');

describe("Our first unit test", () => {
    
    before(() => {
        console.log("Test starting");
    });
    it("Can add 1 and 2 together", () => {
        expect(mylib.add(1,2)).equal(3, "1 + 2 is not 3, for some reason");
    });
    after(() => {
        console.log("Testistng completed!");
    });    
});

describe("a", () => {
    it("zero divided by zero is error!", () => {
        expect(mylib.divide(0,0));
    });        
});
